var bodyParser = require('body-parser')
var mongoose = require('mongoose')
var credential = require('../credentials.js')

mongoose.connect(
  credential.mongodb_url,{ useNewUrlParser: true }
)

var todoSchema = new mongoose.Schema({
  item: String
})

var Todo = mongoose.model('Todo', todoSchema)


var urlencodedParser = bodyParser.urlencoded({ extended: false })
module.exports = function(app) {
  app.get('/todo', function(req, res) {
    Todo.find({}, function(err, data) {
      if (err) throw err
      res.render('todo', { todos: data })
    })
  })

  app.post('/todo', urlencodedParser, function(req, res) {
    var newTodo = Todo(req.body).save(function(err, data) {
      res.json(data)
    })
  })
  /*data.push(req.body);
    res.json(data);*/

  app.delete('/todo/:item', function(req, res) {
    Todo.find({ item: req.params.item.replace(/\-/g, ' ') }).remove(function(
      err,
      data
    ) {
      if (err) throw err
      res.json(data)
    })
    /*data = data.filter(function(todo){
      return todo.item.replace(/ /g, '-') !== req.params.item;
    });
    res.json(data);*/
  })
}
